import os

from logger import get_logger

log = get_logger()


def get_pre_trained_model(in_model_id, last_run_file, model_id):
    if in_model_id is not None: # Use pre-trained model
        if in_model_id == 'last':
            try:
                # Read instance-id from file and load that model
                with open(last_run_file, 'r') as f:
                    pre_trained_model_id = f.read()
            except:
                log.error('Unable to read instance-id from file: {:s}'.format(last_run_file))
                exit(0)  # Can't proceed
        else:
            # instance-id is specified in args
            pre_trained_model_id = in_model_id

        return os.path.join('models', pre_trained_model_id + '_' + model_id + '.model')
    else:
        return None


def getModelInitParams(instance_id, args):
    initParams = {}

    # ------------ Common Params ------------
    Params = {}
    '''
    Available models:
        lag_00:
        lstm_00:
        rnn_00:
    '''
    Params['modelID'] = 'lstm_00'
    Params['phases'] = args.phases
    Params['train_portion'] = 0.7  # % of data to be used for training
    Params['validation_portion'] = 0.1  # % of data to be used for validation
    Params['experiment_mode'] = True  # Some experimentation code might get activated. Default: False
    Params['instance_id'] = instance_id
    Params['temp_dir'] = 'temp'  # Directory to store temporary files
    Params['pred_plot'] = {'display': False, 'write': True, 'dir': Params['temp_dir']}
    Params['pred_plot_detrend'] = {'display': False, 'write': True, 'dir': Params['temp_dir']}

    # File to store info regarding last executed run
    # E.g. used for re-loading the model created in last run
    Params['last_run_file'] = os.path.join(Params['temp_dir'], 'last_run.txt')

    initParams['common'] = Params

    # ------------ modelID: lag_00  ------------
    """
    Next value = last value
    """
    Params = {}
    Params['detrend_method'] = '00'
    Params['pred_plot_detrend'] = {'display': False, 'write': True, 'dir': initParams['common']['temp_dir']}
    initParams['lag_00'] = Params

    # ------------ modelID: lag_01  ------------
    """
    Next value = last value + last change
    """
    Params = {}
    Params['detrend_method'] = '01'
    Params['pred_plot_detrend'] = {'display': False, 'write': True, 'dir': initParams['common']['temp_dir']}
    initParams['lag_01'] = Params

    # ------------------------------------------

    # ------------ modelID: cnn_00 ------------
    Params = {}
    Params['detrend_method'] = '02'
    Params['maxNumEpochs'] = 100
    Params['numUnits'] = 5
    Params['timeSteps'] = 20
    Params['lossFunction'] = 'mse'
    Params['optimizerMethod'] = 'rmsprop'
    Params['metrics'] = ['mse']
    Params['batchSize'] = 256
    Params['oneStepForecast'] = True  # prediction is made using all previous ground truth
    Params['model_path'] = os.path.join('models', instance_id + '_' + initParams['common']['modelID'] + '.model')
    Params['learning_curve'] = {'display': False, 'write': True, 'dir': initParams['common']['temp_dir']}

    Params['pre_trained_model'] = get_pre_trained_model(
        args.in_model_id, initParams['common']['last_run_file'], initParams['common']['modelID'])

    initParams['cnn_00'] = Params
    # ------------------------------------------


    # ------------ modelID: lstm_00 ------------
    Params = {}
    Params['detrend_method'] = '02'
    Params['maxNumEpochs'] = 10
    Params['numUnits'] = 5
    Params['timeSteps'] = 10
    Params['lossFunction'] = 'mse'
    Params['optimizerMethod'] = 'rmsprop'
    Params['metrics'] = ['mse']
    Params['batchSize'] = 256
    Params['oneStepForecast'] = True  # prediction is made using all previous ground truth
    Params['model_path'] = os.path.join('models', instance_id + '_' + initParams['common']['modelID'] + '.model')
    Params['learning_curve'] = {'display': False, 'write': True, 'dir': initParams['common']['temp_dir']}

    Params['pre_trained_model'] = get_pre_trained_model(
        args.in_model_id, initParams['common']['last_run_file'], initParams['common']['modelID'])

    initParams['lstm_00'] = Params

    # -----------------------------------------

    # ------------ modelID: rnn_00 ------------
    Params = {}
    Params['detrend_method'] = '02'
    Params['maxNumEpochs'] = 2
    Params['numUnits'] = 5
    Params['timeSteps'] = 10
    Params['lossFunction'] = 'mse'
    Params['optimizerMethod'] = 'rmsprop'
    Params['metrics'] = ['mse']
    Params['batchSize'] = 256
    Params['oneStepForecast'] = True  # prediction is made using all previous ground truth
    Params['model_path'] = os.path.join('models', instance_id + '_' + initParams['common']['modelID'] + '.model')
    Params['learning_curve'] = {'display': False, 'write': True, 'dir': initParams['common']['temp_dir']}

    Params['pre_trained_model'] = get_pre_trained_model(
        args.in_model_id, initParams['common']['last_run_file'], initParams['common']['modelID'])

    initParams['rnn_00'] = Params

    # -----------------------------------------

    return initParams