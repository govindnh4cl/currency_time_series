from abc import ABC, abstractmethod

from data import Data
from logger import get_logger

log = get_logger()

class BaseModel(ABC):
    def __init__(self, initParams):
        self.commonParams = initParams['common']
        self.modelID = self.commonParams['modelID']
        self.myParams = initParams[self.modelID]
        self.data = Data(initParams)
        self.fullX = None  # Entire time-series
        self.train_start_idx = None
        self.train_end_idx = None
        self.validation_start_idx = None
        self.validation_end_idx = None
        self.test_start_idx = None
        self.test_end_idx = None

    @abstractmethod
    def setupModel(self, X, initParams):
        """
        Some derived models required this step
        :param X:
        :param initParams:
        :return:
        """
        raise NotImplementedError

    @abstractmethod
    def train(self):
        raise NotImplementedError

    @abstractmethod
    def test(self):
        raise NotImplementedError