import os
import numpy as np
import keras
from keras.models import load_model
from keras.callbacks import ModelCheckpoint, Callback
from abc import abstractmethod

from model_base import BaseModel
from eval import assesPredictionResults
from display import display_learning_curve
from logger import get_logger

log = get_logger()

class NNBase(BaseModel):
    def __init__(self, initParams):
        BaseModel.__init__(self, initParams)
        self.tableX = None
        self.y = None
        self.m = None
        self.history = None

    def get_model(self):
        if self.myParams['pre_trained_model'] is None:
            return self.define_model()
        else:
            try:
                # Read existing model saved on disk during previous runs
                m = load_model(self.myParams['pre_trained_model'])
                log.info('Loaded pre-trained model from: {:s}'.format(self.myParams['pre_trained_model']))
            except:
                log.critical('Unable to load pre-trained-model: {:s}'.format(self.myParams['pre_trained_model']))
                raise
            return m

    @abstractmethod
    def define_model(self): # To override
        raise NotImplementedError

    @abstractmethod
    def get_optimizer(self): # To override
        raise NotImplementedError

    def print_config(self):
        log.info('Summary:')
        log.info('=====================================')
        log.info('Indices: Training [{:d}:{:d}] Val [{:d}:{:d}] Test [{:d}:{:d}]'.format(
            self.train_start_idx, self.train_end_idx,
            self.validation_start_idx, self.validation_end_idx,
            self.test_start_idx, self.test_end_idx))
        log.info(self.commonParams)
        log.info(self.myParams)
        keras.utils.print_summary(self.m, print_fn=log.info)
        log.info('=====================================')

    def setupModel(self):
        if self.commonParams['modelID'] != self.modelID:
            log.critical('Wrong model {:} running. Had configured: {:}'\
                         .format(self.modelID, self.commonParams['modelID']))
            exit(0)

        self.fullX = self.data.get_data()
        self.m = self.get_model() # Get model architecture

        self.tableX, self.y, indices = \
            self.data.get_tabled_data(self.fullX, self.myParams['timeSteps'], self.myParams['batchSize'], self.commonParams)

        self.train_start_idx = indices[0]
        self.train_end_idx = indices[1]
        self.validation_start_idx = indices[2]
        self.validation_end_idx = indices[3]
        self.test_start_idx = indices[4]
        self.test_end_idx = indices[5]

        self.print_config()

    class LoggingCallback(Callback):
        """Callback that logs message at end of epoch.
        """
        def __init__(self):
            Callback.__init__(self)

        def on_epoch_end(self, epoch, logs=None):
            if logs is None:
                logs = dict()

            info_msg = list()
            info_msg.append('Epoch: {:d} Loss: {:.3f} Val_loss: {:.3f}'
                            .format(epoch, logs.get('loss', np.nan), logs.get('val_loss', np.nan)))
            for msg in info_msg:
                log.info(msg)

    def train(self):
        log.info('Now training.')
        if self.myParams['pre_trained_model'] is None:
            self.m.compile(loss=self.myParams['lossFunction'],
                           optimizer=self.get_optimizer(),
                           metrics=self.myParams['metrics'])

        cb_store_model = ModelCheckpoint(self.myParams['model_path'], monitor='loss', verbose=0, save_best_only=True)
        cb_logging = self.LoggingCallback()

        hist = self.m.fit(self.tableX[self.train_start_idx:self.train_end_idx + 1],
                          self.y[self.train_start_idx:self.train_end_idx + 1],
                          validation_data=(self.tableX[self.validation_start_idx:self.validation_end_idx + 1],
                                           self.y[self.validation_start_idx:self.validation_end_idx + 1]),
                          batch_size=self.myParams['batchSize'],
                          epochs=self.myParams['maxNumEpochs'],
                          shuffle=False,
                          callbacks=[cb_store_model, cb_logging],
                          verbose=0)

        self.history = hist.history
        log.info('Done training.')
        log.info("Stored model to file: {:s}".format(self.myParams['model_path']))

        # Save last-run instance-id on a file. Will be used for loading last-run model if required
        try:
            with open(self.commonParams['last_run_file'], 'w') as f:
                f.write(self.commonParams['instance_id'])
        except:
            # Print the log. Continue without blocking
            log.error('Unable to write instance-id to file: {:s}'.format(self.commonParams['last_run_file']))

        display_learning_curve(self.myParams['learning_curve'],self.history, block=False)

    def test(self):
        log.info('Now testing.')
        # Load saved model
        if self.commonParams['phases'] == 'test':
            log.info('Loading model from: {:s}'.format(self.myParams['pre_trained_model']))
            self.m = load_model(self.myParams['pre_trained_model'])
        else:
            log.info('Loading model from: {:s}'.format(self.myParams['model_path']))
            self.m = load_model(self.myParams['model_path']) # Read saved model
        self.m.reset_states() # Just for precaution

        start_idx = self.train_start_idx
        end_idx = self.test_end_idx

        y_pred = self.m.predict(self.tableX[start_idx:end_idx + 1], batch_size=self.myParams['batchSize'])
        y_pred = y_pred.reshape(y_pred.shape[0]) # Remove the additional dummy dimension

        assesPredictionResults(self, self.y[start_idx:end_idx + 1], y_pred)
