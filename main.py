seed = 1337

import numpy as np
np.random.seed(seed)
import tensorflow as tf
tf.set_random_seed(seed)
from datetime import datetime
import matplotlib.pyplot as plt
import argparse

from model_lag import LagModel
from model_cnn import CNNModel
from model_lstm import LSTMModel
from model_rnn import Rnn
from config import getModelInitParams
from logger import setup_logger


instance_id = datetime.now().strftime("%Y_%b_%d_%H-%M-%S")  # A time-stamp for log file, model disk-write etc
log = setup_logger(instance_id)

def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-m', '--in_model_id', type=str,
                        help='instance id to reload model from. '
                             '"last" to load from previous run')
    parser.add_argument('-p', '--phases', type=str,
                        help='Phases. Either of "train", "test", "train_test"',
                        required=True)
    return parser.parse_args()

def getModel(initParams):
    model_dict = {
        'lag_00': LagModel,
        'lag_01': LagModel,
        'cnn_00': CNNModel,
        'lstm_00': LSTMModel,
        'rnn_00': Rnn,
    }

    if initParams['common']['modelID'] not in model_dict.keys():
        log.critical("Bad model ID: {:s}".format(initParams['common']['modelID']))
        exit(0)
    model = model_dict[initParams['common']['modelID']](initParams)
    model.setupModel()
    return model

if __name__ == '__main__':
    args = parse_arguments()
    initParams = getModelInitParams(instance_id, args)
    model = getModel(initParams)

    if (initParams['common']['phases'] == 'train') or (initParams['common']['phases'] == 'train_test'):
        model.train() # Training
    if (initParams['common']['phases'] == 'test') or (initParams['common']['phases'] == 'train_test'):
        model.test() # Testing

    plt.show(block=True)  # Block to make sure plots stay on
