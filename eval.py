import numpy as np

from display import display_series
from logger import get_logger

log = get_logger()


def assesPredictionResults(model, y_true_detrend, y_pred_detrend):
    if 'timeSteps' not in model.myParams:
        y_true = model.data.retrend_series(y_true_detrend, 0)
        y_pred = model.data.retrend_series(y_pred_detrend, 0)
    else:
        y_true = model.data.retrend_series(y_true_detrend, model.myParams['timeSteps'])
        y_pred = model.data.retrend_series(y_pred_detrend, model.myParams['timeSteps'])

    if y_true.shape != y_pred.shape:
        log.critical('Predicted series shape {:} does not match with Ground Truth shape {:}'\
            .format(y_pred.shape, y_true.shape))
        exit(0)

    mse_train = np.average((y_true[model.train_start_idx:model.train_end_idx+1] \
                            - y_pred[model.train_start_idx:model.train_end_idx+1])**2)
    mse_validation = np.average((y_true[model.validation_start_idx:model.validation_end_idx+1] \
                            - y_pred[model.validation_start_idx:model.validation_end_idx+1])**2)
    mse_test = np.average((y_true[model.test_start_idx:model.test_end_idx+1] \
                            - y_pred[model.test_start_idx:model.test_end_idx+1])**2)

    mse_train_detrend = np.average((y_true_detrend[model.train_start_idx:model.train_end_idx+1] \
                                    - y_pred_detrend[model.train_start_idx:model.train_end_idx+1])**2)
    mse_validation_detrend = np.average((y_true_detrend[model.validation_start_idx:model.validation_end_idx + 1] \
                                    - y_pred_detrend[model.validation_start_idx:model.validation_end_idx + 1]) ** 2)
    mse_test_detrend = np.average((y_true_detrend[model.test_start_idx:model.test_end_idx+1] \
                                   - y_pred_detrend[model.test_start_idx:model.test_end_idx+1])**2)

    log.info("=== Evaluation Summary (MSE) ===")
    log.info('Phase     #Samples     Original       De-trended')
    log.info("Train       {:5d}     {:7.3f}          {:7.3f}"\
             .format(model.validation_end_idx - model.train_start_idx + 1, mse_train, mse_train_detrend))
    log.info("Validation  {:5d}     {:7.3f}          {:7.3f}"\
             .format(model.validation_end_idx - model.validation_start_idx + 1, mse_validation, mse_validation_detrend))
    log.info("Test        {:5d}     {:7.3f}          {:7.3f}"\
             .format(model.test_end_idx - model.test_start_idx + 1, mse_test, mse_test_detrend))

    display_series(model.commonParams['pred_plot'],
                   [y_true_detrend, y_pred_detrend], ['Ground Truth', 'Predicted'],
                   xlabel='Time -->', ylabel='1 USD in INR -->',
                   title='Detrended Series',
                   train_test_boundary=(model.train_end_idx, model.validation_end_idx),
                   block=False)

    display_series(model.commonParams['pred_plot'],
                   [y_true, y_pred], ['Ground Truth', 'Predicted'],
                   xlabel='Time -->', ylabel='1 USD in INR -->',
                   title='Original Series',
                   train_test_boundary=(model.train_end_idx, model.validation_end_idx),
                   block=False)