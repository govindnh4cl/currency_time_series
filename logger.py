import os
import logging

name = ''

def setup_logger(instance_id):
    log = logging.getLogger(name)
    formatter = logging.Formatter('{asctime:15s} {levelname:8s} {message}', style='{')
    log.setLevel(logging.DEBUG)

    log_dir = 'logs'
    if not os.path.exists(log_dir):
        os.mkdir(log_dir)

    log_file = os.path.join(log_dir, instance_id + ".log")
    fh = logging.FileHandler(log_file)
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)

    log.addHandler(fh)
    log.addHandler(ch)

    log.info('Dumping logs to file: {:s}'.format(log_file))
    return log

def get_logger():
    return logging.getLogger(name)


