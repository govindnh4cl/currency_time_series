import os
import numpy as np
import pandas

from logger import get_logger

log = get_logger()

class Data():
    def __init__(self, initParams):
        self.detrend_param = dict()
        self.detrend_param['method'] = initParams[initParams['common']['modelID']]['detrend_method']
        self.train_portion = initParams['common']['train_portion']

    def _detrend_series(self, series_in):
        if self.detrend_param['method'] == '00':  # Keep as is
            return series_in
        elif self.detrend_param['method'] == '01':  # Just subtract previous sample from current one
            key = self.detrend_param['method']
            self.detrend_param[key] = dict()
            # Store the base number to be able to recreate the series
            self.detrend_param[key]['base'] = series_in[:-1]
            series_out = series_in[1:] - series_in[:-1]
            return series_out
        elif self.detrend_param['method'] == '02':  # subtract previous sample from current one and normalize
            key = self.detrend_param['method']
            self.detrend_param[key] = dict()
            # Store the base number to be able to recreate the series
            self.detrend_param[key]['base'] = series_in[:-1]
            series_out = series_in[1:] - series_in[:-1]
            approx_train_samples = int(len(series_out) * self.train_portion)  # To normalize data
            self.detrend_param[key]['mean'] = series_out[:approx_train_samples].mean()
            self.detrend_param[key]['std'] = series_out[:approx_train_samples].std()
            series_out = (series_out - self.detrend_param[key]['mean']) / self.detrend_param[key]['std']
            return series_out

    def retrend_series(self, series_in, base_skip_count):
        """
        Removes the de-trending effects and creates the actual series
        :param series_in:
        :param base_skip_count: Padding at the start of series_in
        :return:
        """
        if self.detrend_param['method'] == '00':
            return series_in
        elif self.detrend_param['method'] == '01':
            key = self.detrend_param['method']
            series_out = np.zeros(len(series_in))
            for i, val in enumerate(series_in):
                series_out[i] = series_in[i] + self.detrend_param[key]['base'][base_skip_count + i]
            return series_out
        elif self.detrend_param['method'] == '02':
            key = self.detrend_param['method']
            series = (series_in * self.detrend_param[key]['std']) + self.detrend_param[key]['mean']
            series_out = np.zeros(len(series))
            for i, val in enumerate(series):
                series_out[i] = series[i] + self.detrend_param[key]['base'][base_skip_count + i]
            return series_out


    def get_data(self):
        # Read data from csv
        filepath = os.path.join('dataset', 'USD_INR.csv')
        if not os.path.exists(filepath):
            log.critical("File not found: {:s}".format(filepath))
            exit(0)

        df_original = pandas.read_csv(filepath)
        series_original = df_original['Price'][::-1].values  # flip it
        series = self._detrend_series(series_original)
        return series

    @staticmethod
    def get_tabled_data(X, time_steps, batch_size, commonParams):
        # Create dataset
        tableX = np.zeros([len(X) - time_steps, time_steps])
        y = np.zeros(tableX.shape[0])
        for i in range(tableX.shape[0]):
            tableX[i] = X[i:i + time_steps].reshape(time_steps)
            y[i] = X[i + time_steps]

        # Reshaping to add additional dimension for data_dim in LSTM layer
        tableX = tableX.reshape(tableX.shape + (1,))

        # Truncating the input to make it a multiple of batch size
        tableX = tableX[0:int(tableX.shape[0] / batch_size) * batch_size]
        y = y[0:int(y.shape[0] / batch_size) * batch_size]

        # (for testing purposes) shifting the labels so that input sample contains the output
        if 0 and commonParams['experiment_mode']:
            y = np.concatenate([tableX[0][-1], y[:-1]])

        # Divide data into train and test partitions
        numBatches = tableX.shape[0] / batch_size
        train_start_idx = 0
        train_end_idx = int(numBatches * commonParams['train_portion']) * batch_size - 1
        validation_start_idx = train_end_idx + 1
        validation_end_idx = validation_start_idx + int(numBatches * commonParams['validation_portion']) * batch_size - 1
        test_start_idx = validation_end_idx + 1
        test_end_idx = tableX.shape[0] - 1

        return tableX, y, (train_start_idx, train_end_idx, validation_start_idx,
                           validation_end_idx, test_start_idx, test_end_idx)







