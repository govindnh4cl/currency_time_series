from keras.models import Sequential
from keras.layers import Dense, SimpleRNN
from keras.optimizers import RMSprop, Adagrad

from model_nn_base import NNBase
from logger import get_logger

log = get_logger()

class Rnn(NNBase):
    def __init__(self, initParams):
        NNBase.__init__(self, initParams)

    # Override base class implementation
    def define_model(self):
        m = Sequential()
        if 0: # stateful
            m.add(SimpleRNN(self.myParams['numUnits'], batch_input_shape=(self.myParams['batchSize'], self.myParams['timeSteps'], 1), stateful=True, return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], stateful=True))
            m.add(Dense(1))
        elif 0:  # No bias
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False, batch_input_shape=(self.myParams['batchSize'], self.myParams['timeSteps'], 1), return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False))
            m.add(Dense(1))
        elif 0:  # Dense
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False, batch_input_shape=(self.myParams['batchSize'], self.myParams['timeSteps'], 1), return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False))
            m.add(Dense(self.myParams['numUnits']))
            m.add(Dense(1))
        elif 1:  # Deep
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False, batch_input_shape=(self.myParams['batchSize'], self.myParams['timeSteps'], 1), return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False, return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False, return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits'], use_bias=False))
            m.add(Dense(self.myParams['numUnits']))
            m.add(Dense(self.myParams['numUnits']))
            m.add(Dense(1))
        else:
            m.add(SimpleRNN(self.myParams['numUnits'], batch_input_shape=(self.myParams['batchSize'], self.myParams['timeSteps'], 1), return_sequences=True))
            m.add(SimpleRNN(self.myParams['numUnits']))
            m.add(Dense(1))
        return m

    def get_optimizer(self):
        if self.myParams['optimizerMethod'] == 'rmsprop':
            opt = RMSprop(lr = 0.02)
        elif self.myParams['optimizerMethod'] == 'adagrad':
            opt = Adagrad(lr = 0.05)
        else:
            opt = None
        return opt