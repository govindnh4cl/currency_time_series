import os
import matplotlib.pyplot as plt

from logger import get_logger

log = get_logger()

def display_learning_curve(option_dict, history, xlabel='Epochs -->', ylabel='MSE -->', block=True):
    if True not in option_dict.values():
        return

    plt.figure()
    if 'loss' in history:
        plt.plot(history['loss'], linewidth=1, color='red', label='Train Loss')
    if 'val_loss' in history:
        plt.plot(history['val_loss'], linewidth=1, color='blue', label='Val Loss')
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()
    plt.legend()

    if option_dict['write'] is True:
        filepath = os.path.join(option_dict['dir'], 'Learning curve.png')
        log.info('Dumping plot to {:s}'.format(filepath))
        plt.savefig(filepath)

    if option_dict['display'] is True:
        plt.show(block=block)
    else:
        plt.close()  # Close the plot so that it never gets displayed


def display_series(option_dict, series_list, label_list, ylabel='', xlabel='', title='', train_test_boundary=None, block=True):
    if True not in option_dict.values():
        return

    plt.figure()
    for i, series in enumerate(series_list):
        plt.plot(series, linewidth=1, label=label_list[i])

    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()
    plt.legend()
    if train_test_boundary is not None:
        plt.axvline(x=train_test_boundary[0], color='purple', linestyle='--', linewidth=1)
        plt.text(train_test_boundary[0], 20, 'Train/Val', verticalalignment='bottom', color='purple')

        plt.axvline(x=train_test_boundary[1], color='purple', linestyle='--', linewidth=1)
        plt.text(train_test_boundary[1], 15, 'Val/Test', verticalalignment='bottom', color='purple')

    if option_dict['write'] is True:
        filepath = os.path.join(option_dict['dir'], title + '.png')
        log.info('Dumping plot to {:s}'.format(filepath))
        plt.savefig(filepath)

    if option_dict['display'] is True:
        plt.show(block=block)
    else:
        plt.close()  # Close the plot so that it never gets displayed


