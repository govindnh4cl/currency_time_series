import numpy as np

from eval import assesPredictionResults
from model_base import BaseModel
from logger import get_logger

log = get_logger()

class LagModel(BaseModel):
    """
    Directly uses latest value.
    """
    def __init__(self, initParams):
        BaseModel.__init__(self, initParams)

    def setupModel(self):
        self.fullX = self.data.get_data()
        numSamples = len(self.fullX)
        self.train_start_idx = 0
        self.train_end_idx = int(numSamples * self.commonParams['train_portion'])
        self.validation_start_idx = self.train_end_idx + 1
        self.validation_end_idx = self.validation_start_idx + int(numSamples * self.commonParams['validation_portion'])
        self.test_start_idx = self.validation_end_idx + 1
        self.test_end_idx = numSamples - 1

    def train(self):
        """
        Lag model does not require any training
        :return: None
        """
        pass

    def test(self):
        start_idx = self.train_start_idx
        end_idx = self.test_end_idx

        # Return time-shifted values
        shift = 1
        predX = np.zeros(end_idx - start_idx + 1)
        for i in range(shift):
            predX[i] = self.fullX[shift - 1]
        predX[shift:] =  self.fullX[start_idx:end_idx + 1 - shift]

        assesPredictionResults(self, self.fullX[start_idx:end_idx+1], predX)