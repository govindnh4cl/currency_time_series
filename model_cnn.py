import os
import numpy as np
from keras.models import load_model
from keras.callbacks import ModelCheckpoint
from keras.models import Model
from keras.layers import Input, Dense, Conv1D, Flatten
from keras.optimizers import RMSprop, Adagrad

# User defined imports
from model_nn_base import NNBase
from eval import assesPredictionResults
from display import display_learning_curve
from logger import get_logger

log = get_logger()

class CNNModel(NNBase):
    def __init__(self, initParams):
        NNBase.__init__(self, initParams)

    # Override base class implementation
    def define_model(self):
        if 1:
            inp = Input(shape=(self.myParams['timeSteps'], 1))
            x = Conv1D(8, self.myParams['numUnits'], activation='tanh', padding='same')(inp)
            x = Conv1D(8, self.myParams['numUnits'], activation='tanh', padding='same')(x)
            x = Conv1D(8, self.myParams['numUnits'], activation='tanh', padding='same')(x)
            x = Flatten()(x)
            x = Dense(32, activation='tanh')(x)
            out = Dense(1)(x)

        m = Model(inputs=inp, outputs=out)
        return m

    def get_optimizer(self):
        if self.myParams['optimizerMethod'] == 'rmsprop':
            opt = RMSprop(lr = 0.005)
        elif self.myParams['optimizerMethod'] == 'adagrad':
            opt = Adagrad(lr = 0.05)
        else:
            opt = None
        return opt
